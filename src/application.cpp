#include <application.hpp>
#include <stdexcept>
#include <thread>
#include <iostream>
#include <functional>

std::unique_ptr<Application> Application::instance_;

std::vector<VkExtensionProperties> get_available_extensions() {
	uint32_t count = 0;
	std::vector<VkExtensionProperties> result;
	if (vkEnumerateInstanceExtensionProperties(nullptr, &count, nullptr) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to get supported extensions");
	}
	if (!count) {
		return result;
	}
	result.resize(count);
	if (vkEnumerateInstanceExtensionProperties(nullptr, &count, result.data()) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to get supported extensions");
	}
	return result;
}

VulkanInstance::VulkanInstance() {
	auto available_extensions = get_available_extensions();
	std::vector<const char*> extensions;
	extensions.push_back(VK_KHR_SURFACE_EXTENSION_NAME);
	extensions.push_back(VK_KHR_XCB_SURFACE_EXTENSION_NAME);
	for (auto it = extensions.begin(); it != extensions.end(); ++it) {
		auto it2 = std::find_if(available_extensions.begin(), available_extensions.end(),
			[&](VkExtensionProperties prop)->bool{ return std::strcmp(*it, prop.extensionName) == 0; });
		if (it2 == available_extensions.end()) {
			throw std::runtime_error("Error: Required extension '" + std::string(*it) + "' is not supported");
		}
	}
	VkApplicationInfo app_info{};
	app_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	app_info.applicationVersion = 1;
	app_info.engineVersion = 1;
	app_info.pApplicationName = "";
	app_info.pEngineName = "";
	VkInstanceCreateInfo instance_info{};
	instance_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	instance_info.pApplicationInfo = nullptr;
	instance_info.enabledExtensionCount = extensions.size();
	instance_info.ppEnabledExtensionNames = extensions.data();

	if (vkCreateInstance(&instance_info, nullptr, &instance_) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to create Vulkan instance");
	}
}

VulkanInstance::~VulkanInstance() {
	if (instance_ != VK_NULL_HANDLE) {
		vkDestroyInstance(instance_, nullptr);
		instance_ = VK_NULL_HANDLE;
	}
}

VkInstance VulkanInstance::handle() const {
	return instance_;
}

VulkanInstance::operator bool() const {
	return instance_ != VK_NULL_HANDLE;
}

Application::Application(int argc, const char** argv) {

}

Application::~Application() {

}

struct Vertex {
	float u, v, w, x, y, z;
};

int Application::run(int argc, const char** argv) {
	instance_ = std::unique_ptr<Application>(new Application(argc, argv));
	instance_->device_ = std::unique_ptr<Device>(new Device());

	std::vector<Vertex> vertices = {
		{1.0f, 0.0f, 0.0f, -0.5f, -0.5f, 0.0f},
		{0.0f, 1.0f, 0.0f, 0.5f, -0.5f, 0.0f},
		{0.0f, 0.0f, 1.0f, 0.5f, 0.5f, 0.0f},
		{1.0f, 1.0f, 1.0f, -0.5f, 0.5f, 0.0f}
	};
	
	std::vector<uint32_t> indexes = {
		0, 1, 2, 2, 3, 0
	};

	int size = sizeof(Vertex) * vertices.size();
	int indexes_size = sizeof(uint32_t) * indexes.size();

	DeviceMemory host_memory;
	host_memory.add_buffer(
			VK_BUFFER_USAGE_TRANSFER_SRC_BIT, 
			size, 
			VK_SHARING_MODE_EXCLUSIVE)
		.add_buffer(
			VK_BUFFER_USAGE_TRANSFER_SRC_BIT, 
			indexes_size, 
			VK_SHARING_MODE_EXCLUSIVE)
		.allocate(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT)
		.map(vertices.data(), 0, size);
	host_memory.map(indexes.data(), host_memory.buffer_offsets().back(),indexes_size);

	instance_->gpu_memory_ = std::unique_ptr<DeviceMemory>(new DeviceMemory());
	instance_->gpu_memory_->add_buffer(
			VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, 
			size, 
			VK_SHARING_MODE_EXCLUSIVE)
		.add_buffer(
			VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, 
			indexes_size, 
			VK_SHARING_MODE_EXCLUSIVE)
		.allocate(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	{
		CommandPool temp;
		temp.allocate_buffers(1);
		VkBuffer source = host_memory.buffers().front();
		VkBuffer dest = instance_->gpu_memory_->buffers().front();
		temp.record(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT, 
			[&](VkCommandBuffer cmd_buff) -> void {
				VkBufferCopy copy = {};
				copy.size = size;
				vkCmdCopyBuffer(cmd_buff, source, dest, 1, &copy);
			});
		Fence fence;
		SubmitQueue sq(instance_->device_->queues().front());
		sq.command_buffers({temp.buffers().front()});
		if (sq.submit(fence.handle()) != VK_SUCCESS) {
			throw std::runtime_error("Failed to submit queue");
		}
		fence.wait();
	}

	{
		CommandPool temp;
		temp.allocate_buffers(1);
		VkBuffer source = host_memory.buffers().back();
		VkBuffer dest = instance_->gpu_memory_->buffers().back();
		temp.record(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT, 
			[&](VkCommandBuffer cmd_buff) -> void {
				VkBufferCopy copy = {};
				copy.size = indexes_size;
				vkCmdCopyBuffer(cmd_buff, source, dest, 1, &copy);
			});
		Fence fence;
		SubmitQueue sq(instance_->device_->queues().front());
		sq.command_buffers({temp.buffers().front()});
		if (sq.submit(fence.handle()) != VK_SUCCESS) {
			throw std::runtime_error("Failed to submit queue");
		}
		fence.wait();
	}

	instance_->cmd_pool_ = std::unique_ptr<CommandPool>(new CommandPool());
	instance_->layouts_.push_back(std::move(DescriptorSetLayout()
		.add_binding(0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, VK_SHADER_STAGE_VERTEX_BIT).create()));

	instance_->update_swapchain();
	instance_->running_ = true;
	while (instance_->running_) {
		instance_->render_loop();
	}
	vkDeviceWaitIdle(instance_->device_->handle());
	return EXIT_SUCCESS;
}

const VulkanInstance& Application::instance() {
	if (!instance_ || !instance_->vulkan_instance_) {
		throw std::runtime_error("Error: Invalid Vulkan instance");
	}
	return instance_->vulkan_instance_;
}

Window& Application::window() {
	if (!instance_ || !instance_->window_.connection()) {
		throw std::runtime_error("Error: Invalid display");
	}
	return instance_->window_;
}

const Device& Application::device() {
	if (!instance_ || !instance_->device_) {
		throw std::runtime_error("Error: Invalid device");
	}
	return *(instance_->device_);
}

const DeviceMemory& Application::memory() {
	if (!instance_ || !instance_->gpu_memory_) {
		throw std::runtime_error("Error: Invalid device memory");
	}
	return *(instance_->gpu_memory_);
}

const Swapchain& Application::swapchain() {
	if (!instance_ || !instance_->swapchain_) {
		throw std::runtime_error("Error: Invalid swapchain");
	}
	return *(instance_->swapchain_);
}

Pipeline& Application::pipeline() {
	if (!instance_ || !instance_->pipeline_) {
		throw std::runtime_error("Error: Invalid pipeline");
	}
	return *(instance_->pipeline_);
}

const CommandPool& Application::command_pool() {
	if (!instance_ || !instance_->cmd_pool_) {
		throw std::runtime_error("Error: Invalid command pool");
	}
	return *(instance_->cmd_pool_);
}

const std::vector<Framebuffer>& Application::frame_buffers() {
	if (!instance_) {
		throw std::runtime_error("Error: Invalid instance");
	}
	return instance_->framebuffers_;
}

void Application::update_swapchain() {
	framebuffers_.clear();
	views_.clear();
	cmd_pool_ = std::unique_ptr<CommandPool>(new CommandPool());

	swapchain_ = std::unique_ptr<Swapchain>(new Swapchain());
	VkExtent2D extent = swapchain_->extent();
	auto swap_images = swapchain_->images();
	VkFormat format = swapchain_->image_format().format;

	camera_.look_at(glm::vec3(2.0f, 2.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	camera_.perspective(glm::radians(45.0f), extent.width / (float) extent.height, 0.1f, 10.0f);

	ubos_ = std::unique_ptr<DeviceMemory>(new DeviceMemory());
	for (size_t i = 0; i < swap_images.size(); ++i) {
		ubos_->add_buffer(VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, sizeof(MVPMatrix), VK_SHARING_MODE_EXCLUSIVE);
	}
	ubos_->allocate(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

	cmd_pool_->allocate_buffers(swap_images.size());

	std::vector<VkDescriptorSetLayout> layouts(swap_images.size(), layouts_.front().handle());
	pipeline_ = std::unique_ptr<Pipeline>(new Pipeline(layouts));

	pipeline_->set(
		{ 
			ShaderStage(VK_SHADER_STAGE_VERTEX_BIT, "shaders/tri.vert"), 
			ShaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, "shaders/tri.frag") 
		})
		.set(ViewportState(extent))
		.set(VertexInputState()
			.add_binding(0, sizeof(Vertex), VK_VERTEX_INPUT_RATE_VERTEX)
			.add_attribute(0, 0, VK_FORMAT_R32G32B32_SFLOAT, 3 * sizeof(float))
			.add_attribute(0, 1, VK_FORMAT_R32G32B32_SFLOAT, 0))
		.create();

	for (auto it = swap_images.begin(); it != swap_images.end(); ++it) {
		views_.push_back(ImageView(*it, format));
	}

	for (auto it = views_.begin(); it != views_.end(); ++it) {
		framebuffers_.push_back(Framebuffer(
			pipeline_->render_pass().handle(), 
			{it->handle()}, 
			extent));
	}

	
	descriptor_pool_ = std::unique_ptr<DescriptorPool>(new DescriptorPool(
		{ std::make_pair(swap_images.size(), VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER) },
		layouts));
	for (size_t i = 0; i < swap_images.size(); ++i) {
		VkDescriptorBufferInfo info = {};
		info.buffer = ubos_->buffers()[i];
		info.offset = 0;
		info.range = sizeof(MVPMatrix);
		descriptor_pool_->write(i, 0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 0, { info });
	}
	
	Application::pipeline().render_pass().clear_values({{0.0f, 0.0f, 0.0f, 1.0f}});

	cmd_pool_->record(VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT, 
			[&](VkCommandBuffer cmd_buff, int buffer_index)->void {

		Application::pipeline()
			.render_pass()
			.target(Application::frame_buffers()[buffer_index].handle(), {0, 0}, extent)
			.commands(cmd_buff)
			.begin();

		VkBuffer vertex_buffer = memory().buffers().front();
		VkBuffer index_buffer = memory().buffers()[1];

		vkCmdBindPipeline(cmd_buff, VK_PIPELINE_BIND_POINT_GRAPHICS, Application::pipeline().handle());
		vkCmdBindVertexBuffers(cmd_buff, 0, 1, 
			&vertex_buffer,
			Application::memory().buffer_offsets().data());

		vkCmdBindIndexBuffer(cmd_buff, 
			index_buffer, 
			0,
			VK_INDEX_TYPE_UINT32);

		vkCmdBindDescriptorSets(
				cmd_buff, 
				VK_PIPELINE_BIND_POINT_GRAPHICS, 
				Application::pipeline().layout(), 
				0, 
				1, 
				&descriptor_pool_->sets()[buffer_index], 
				0, 
				nullptr);

		vkCmdDrawIndexed(cmd_buff, 6, 1, 0, 0, 0);
		vkCmdEndRenderPass(cmd_buff);
	});
}

void Application::render_loop() {

	auto cmd_buffers = cmd_pool_->buffers();
	std::vector<VkPipelineStageFlags> stages { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
	size_t concurrent_frames = swapchain_->images().size();
	size_t current_frame = 0;
	std::vector<Semaphore> submit_locks(concurrent_frames), present_locks(concurrent_frames);
	std::vector<Fence> frame_locks(concurrent_frames);

	while (running_) {
		current_frame = ++current_frame % concurrent_frames;
		frame_locks[current_frame].wait();

		auto x = window_.events();
		for (Window::Event e : x) {
			if (e == Window::KEY_DOWN) {
				std::cout << window_.width() << " " << window_.height() << "\n";
				running_ = false;
			}
			if (e == Window::EXPOSE) {
				vkDeviceWaitIdle(device_->handle());
				update_swapchain();
				return;
			}
		}
		uint32_t image;
		VkResult result = vkAcquireNextImageKHR(
				device_->handle(), 
				swapchain_->handle(), 
				std::numeric_limits<size_t>::max(), 
				submit_locks[current_frame].handle(), 
				VK_NULL_HANDLE, 
				&image);

		if (result != VK_SUCCESS)  {
			vkDeviceWaitIdle(device_->handle());
			update_swapchain();
			return;
		}

		camera_.update();
		ubos_->map(camera_.ubo(), ubos_->buffer_offsets()[image], sizeof(MVPMatrix));

		SubmitQueue sq(device_->queues().front());
		sq.waits({submit_locks[current_frame].handle()})
			.waits(stages)
			.fires({present_locks[current_frame].handle()})
			.command_buffers({cmd_buffers[image]});
		if (sq.submit(frame_locks[current_frame].handle()) != VK_SUCCESS) {
			throw std::runtime_error("Failed to submit queue");
		}

		PresentQueue pq(device_->queues().back());
		pq.waits({present_locks[current_frame].handle()})
			.swapchains({swapchain_->handle()})
			.images({image});
		switch (pq.present()) {
			case VK_SUCCESS:
				break;
			case VK_ERROR_OUT_OF_DATE_KHR:
			case VK_SUBOPTIMAL_KHR:
				vkDeviceWaitIdle(device_->handle());
				update_swapchain();
				break;
			default:
				throw std::runtime_error("Failed to present queue");
		}
	}
}