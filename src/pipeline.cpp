#include <pipeline.hpp>
#include <fstream>
#include <application.hpp>

ShaderStage::ShaderStage(VkShaderStageFlagBits flags, const std::string &filename) {
	_flags = flags;
	std::ifstream f(filename, std::ios::ate | std::ios::binary);
	if (!f.is_open()) {
		throw std::runtime_error("Could not open file '" + filename + "'");
	}
	size_t size = f.tellg();
	_code.resize(size);
	f.seekg(0);
	f.read(_code.data(), size);
}

VkShaderModuleCreateInfo ShaderStage::info() {
	VkShaderModuleCreateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	info.codeSize = _code.size();
	info.pCode = reinterpret_cast<const uint32_t *> (_code.data());
	return info;
}

VkShaderStageFlagBits ShaderStage::flags() const {
	return _flags;
}

ShaderBuild::~ShaderBuild() {
	for (auto module : _modules) {
		vkDestroyShaderModule(Application::device().handle(), module.second, nullptr);
	}
}

ShaderBuild::ShaderBuild(const std::vector<ShaderStage> &shaders) {
	for (auto shader : shaders) {
		VkShaderModuleCreateInfo info = shader.info();
		VkShaderModule module;
		if (vkCreateShaderModule(Application::device().handle(), &info, nullptr, &module) != VK_SUCCESS) {
			throw std::runtime_error("Error: Failed to build shader module");
		}
		_modules.push_back(std::make_pair(shader.flags(), module));
	}
}

std::vector<VkPipelineShaderStageCreateInfo> ShaderBuild::info() const {
	std::vector<VkPipelineShaderStageCreateInfo> result;
	for (auto module : _modules) {
		VkPipelineShaderStageCreateInfo info = {};
		info.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		info.stage = module.first;
		info.module = module.second;
		info.pName = "main";
		result.push_back(info);
	}
	return result;
}

VertexInputState& VertexInputState::add_binding(uint32_t id, uint32_t stride, VkVertexInputRate rate) {
	VkVertexInputBindingDescription result = {};
	result.binding = id;
	result.stride = stride;
	result.inputRate = rate;
	_bindings.push_back(result);
	return *this;
}

VertexInputState& VertexInputState::add_attribute(
		uint32_t binding, 
		uint32_t location, 
		const VkFormat &format, 
		uint32_t offset) {
	VkVertexInputAttributeDescription result = {};
	result.location = location;
	result.binding = binding;
	result.format = format;
	result.offset = offset;
	_attributes.push_back(result);
	return *this;
}

VkPipelineVertexInputStateCreateInfo VertexInputState::info() const {
	VkPipelineVertexInputStateCreateInfo result = {};
	result.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	result.vertexBindingDescriptionCount = _bindings.size();
	result.pVertexBindingDescriptions = _bindings.data();
	result.vertexAttributeDescriptionCount = _attributes.size();
	result.pVertexAttributeDescriptions = _attributes.data();
	return result;
}

VkPipelineInputAssemblyStateCreateInfo InputAssembly::info() const {
	VkPipelineInputAssemblyStateCreateInfo result = {};
	result.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	result.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	result.primitiveRestartEnable = VK_FALSE;
	return result;
}

ViewportState::ViewportState() : _viewport{}, _scissor{} {

}

ViewportState::ViewportState(const VkExtent2D &extent) : _viewport{}, _scissor{} {
	_viewport.x = 0.0f;
	_viewport.y = 0.0f;
	_viewport.width = (float) extent.width;
	_viewport.height = (float) extent.height;
	_viewport.minDepth = 0.0f;
	_viewport.maxDepth = 1.0f;
	_scissor.offset = {0, 0};
	_scissor.extent = extent;
}

VkPipelineViewportStateCreateInfo ViewportState::info() const {
	VkPipelineViewportStateCreateInfo result = {};
	result.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	result.viewportCount = 1;
	result.pViewports = &_viewport;
	result.scissorCount = 1;
	result.pScissors = &_scissor;
	return result;
}

VkPipelineRasterizationStateCreateInfo RasterizationState::info() const {
	VkPipelineRasterizationStateCreateInfo result = {};
	result.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	result.depthClampEnable = VK_FALSE;
	result.rasterizerDiscardEnable = VK_FALSE;
	result.polygonMode = VK_POLYGON_MODE_FILL;
	result.lineWidth = 1.0f;
	result.cullMode = VK_CULL_MODE_BACK_BIT;
	result.frontFace = VK_FRONT_FACE_CLOCKWISE;
	result.depthBiasEnable = VK_FALSE;
	result.depthBiasConstantFactor = 0.0f;
	result.depthBiasClamp = 0.0f;
	result.depthBiasSlopeFactor = 0.0f;
	return result;
}

VkPipelineMultisampleStateCreateInfo MultisampleState::info() const {
	VkPipelineMultisampleStateCreateInfo result = {};
	result.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	result.sampleShadingEnable = VK_FALSE;
	result.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	result.minSampleShading = 1.0f;
	result.pSampleMask = nullptr;
	result.alphaToCoverageEnable = VK_FALSE;
	result.alphaToOneEnable = VK_FALSE;
	return result;
}

VkPipelineDepthStencilStateCreateInfo DepthStencilState::info() const {
	VkPipelineDepthStencilStateCreateInfo result = {};
	result.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	return result;
}

ColorBlendState::ColorBlendState() : attachment_{} {
	attachment_.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT |
			VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
	attachment_.blendEnable = VK_FALSE;
	attachment_.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
	attachment_.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
	attachment_.colorBlendOp = VK_BLEND_OP_ADD;
	attachment_.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
	attachment_.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	attachment_.alphaBlendOp = VK_BLEND_OP_ADD;
}

VkPipelineColorBlendStateCreateInfo ColorBlendState::info() const {
	VkPipelineColorBlendStateCreateInfo result = {};
	result.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	result.logicOpEnable = VK_FALSE;
	result.logicOp = VK_LOGIC_OP_COPY;
	result.attachmentCount = 1;
	result.pAttachments = &attachment_;
	result.blendConstants[0] = 0.0f;
	result.blendConstants[1] = 0.0f;
	result.blendConstants[2] = 0.0f;
	result.blendConstants[3] = 0.0f;
	return result;
}

DynamicState::DynamicState() : _states{} {
}

DynamicState::DynamicState(const std::vector<VkDynamicState> &states) : _states(states) {

}

VkPipelineDynamicStateCreateInfo DynamicState::info() const {
	VkPipelineDynamicStateCreateInfo result = {};
	result.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	result.dynamicStateCount = _states.size();
	result.pDynamicStates = (_states.empty()) ? nullptr : _states.data();
	return result;
}

PipelineLayout::PipelineLayout(const std::vector<VkDescriptorSetLayout>& layouts):layout_{VK_NULL_HANDLE} {
	VkPipelineLayoutCreateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	info.setLayoutCount = layouts.size();
	info.pSetLayouts = layouts.data();
	info.pushConstantRangeCount = 0;
	info.pPushConstantRanges = nullptr;
	if (vkCreatePipelineLayout(Application::device().handle(), &info, nullptr, &layout_)) {
		throw std::runtime_error("Error: Failed to create pipeline layout");
	}
}

PipelineLayout::~PipelineLayout() {
	if (layout_ != VK_NULL_HANDLE) {
		vkDestroyPipelineLayout(Application::device().handle(), layout_, nullptr);
		layout_ = VK_NULL_HANDLE;
	}
}

VkPipelineLayout PipelineLayout::handle() const {
	return layout_;
}

Pipeline::Pipeline(const std::vector<VkDescriptorSetLayout>& layouts):pipeline_{VK_NULL_HANDLE}, layout_(layouts) {
	//_dynamic({VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_LINE_WIDTH})
}

Pipeline::~Pipeline() {
	if (pipeline_ != VK_NULL_HANDLE) {
		vkDestroyPipeline(Application::device().handle(), pipeline_, nullptr);
		pipeline_ = VK_NULL_HANDLE;
	}
}

std::vector<ShaderStage>& Pipeline::shaders() {
	return _shaders;
}

Pipeline& Pipeline::set(const std::vector<ShaderStage> &shaders) {
	_shaders = shaders;
	return *this;
}


Pipeline& Pipeline::set(const VertexInputState &state) {
	_vertex_input = state;
	return *this;
}

Pipeline& Pipeline::set(const ViewportState &state) {
	_viewport = state;
	return *this;
}

Pipeline& Pipeline::set(const RasterizationState &state) {
	_rasterization = state;
	return *this;
}

Pipeline& Pipeline::set(const MultisampleState &state) {
	_multisample = state;
	return *this;
}

Pipeline& Pipeline::set(const DepthStencilState &state) {
	_depth_stencil = state;
	return *this;
}

Pipeline& Pipeline::set(const ColorBlendState &state) {
	_color_blend = state;
	return *this;
}

Pipeline& Pipeline::set(const DynamicState &state) {
	_dynamic = state;
	return *this;
}

VkPipelineLayout Pipeline::layout() const {
	return layout_.handle();
}

Pipeline& Pipeline::create() {
	ShaderBuild build(_shaders);
	auto shaders = build.info();
	auto vertex_input = _vertex_input.info();
	auto input_assembly = _input_assembly.info();
	auto viewport = _viewport.info();
	auto rasterization = _rasterization.info();
	auto multisample = _multisample.info();
	auto depth_stencil = _depth_stencil.info();
	auto color_blend = _color_blend.info();
	auto dynamic = _dynamic.info();

	VkGraphicsPipelineCreateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	info.stageCount = shaders.size();
	info.pStages = shaders.data();
	info.pVertexInputState = &vertex_input;
	info.pInputAssemblyState = &input_assembly;
	info.pViewportState = &viewport;
	info.pRasterizationState = &rasterization;
	info.pMultisampleState = &multisample;
	info.pDepthStencilState = &depth_stencil;
	info.pColorBlendState = &color_blend;
	info.pDynamicState = &dynamic;
	info.layout = layout_.handle();
	info.renderPass = render_pass_.handle();
	info.subpass = 0;
	info.basePipelineHandle = VK_NULL_HANDLE;
	info.basePipelineIndex = -1;
	VkPipeline result;

	VkPipelineCache cache = VK_NULL_HANDLE;
	if (vkCreateGraphicsPipelines(Application::device().handle(), cache, 1, &info, nullptr, &pipeline_) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to create graphics pipeline");
	}
	return *this;
}

RenderPass& Pipeline::render_pass() {
	return render_pass_;
}

VkPipeline Pipeline::handle() const {
	return pipeline_;
}

DescriptorSetLayout::DescriptorSetLayout():layout_{VK_NULL_HANDLE} {

}

DescriptorSetLayout::~DescriptorSetLayout() {
	if (layout_ != VK_NULL_HANDLE) {
		vkDestroyDescriptorSetLayout(Application::device().handle(), layout_, nullptr);
		layout_ = VK_NULL_HANDLE;
	}
}

DescriptorSetLayout::DescriptorSetLayout(DescriptorSetLayout&& other) {
	layout_ = other.layout_;
	other.layout_ = VK_NULL_HANDLE;
	bindings_ = std::move(other.bindings_);
}

DescriptorSetLayout& DescriptorSetLayout::add_binding(
		uint32_t binding, 
		VkDescriptorType type, 
		uint32_t count, 
		VkShaderStageFlagBits stages) {
	return add_binding(binding, type, count, stages, nullptr);
}

DescriptorSetLayout& DescriptorSetLayout::add_binding(
		uint32_t binding, 
		VkDescriptorType type, 
		uint32_t count, 
		VkShaderStageFlagBits stages,
		const VkSampler* sampler) {
	VkDescriptorSetLayoutBinding x = {};
	x.binding = binding;
	x.descriptorType = type;
	x.descriptorCount = count;
	x.stageFlags = stages;
	x.pImmutableSamplers = sampler;
	bindings_.push_back(x);
	return *this;
}

DescriptorSetLayout& DescriptorSetLayout::create() {
	VkDescriptorSetLayoutCreateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	info.bindingCount = bindings_.size();
	info.pBindings = bindings_.data();
	if (vkCreateDescriptorSetLayout(Application::device().handle(), &info, nullptr, &layout_) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to create descriptor layout");
	}
	return *this;
}

VkDescriptorSetLayout DescriptorSetLayout::handle() const {
	return layout_;
}

DescriptorPool::DescriptorPool(const std::vector<desc_type_count_t>& desc_types, const std::vector<VkDescriptorSetLayout>& layouts)
		:pool_{VK_NULL_HANDLE} {

	std::vector<VkDescriptorPoolSize> sizes;
	for (auto it = desc_types.begin(); it != desc_types.end(); ++it) {
		VkDescriptorPoolSize size = {};
		size.descriptorCount = it->first;
		size.type = it->second;
		sizes.push_back(size);
	}

	VkDescriptorPoolCreateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	info.poolSizeCount = static_cast<uint32_t>(sizes.size());
	info.pPoolSizes = sizes.data();
	info.maxSets = static_cast<uint32_t>(layouts.size());
	if (vkCreateDescriptorPool(Application::device().handle(), &info, nullptr, &pool_) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to create descriptor pool");
	}

	sets_.resize(layouts.size(), VK_NULL_HANDLE);
	VkDescriptorSetAllocateInfo alloc_info = {};
	alloc_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	alloc_info.descriptorPool = pool_;
	alloc_info.descriptorSetCount = layouts.size();
	alloc_info.pSetLayouts = layouts.data();
	if (vkAllocateDescriptorSets(Application::device().handle(), &alloc_info, sets_.data()) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to allocate descriptor sets");
	}
}

DescriptorPool::~DescriptorPool() {
	if (pool_ !=  VK_NULL_HANDLE) {
		vkDestroyDescriptorPool(Application::device().handle(), pool_, nullptr);
		pool_ = VK_NULL_HANDLE;
	}
}

const DescriptorPool& DescriptorPool::write(size_t index, uint32_t binding, VkDescriptorType type, uint32_t elem, const std::vector<VkDescriptorBufferInfo>& buffers) const {
	VkWriteDescriptorSet write = {};
	write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
	write.dstSet = sets_[index];
	write.dstBinding = binding;
	write.dstArrayElement = elem;
	write.descriptorType = type;
	write.descriptorCount = buffers.size();
	write.pBufferInfo = buffers.data();
	vkUpdateDescriptorSets(Application::device().handle(), 1, &write, 0, nullptr);
	return *this;
}

const DescriptorPool& DescriptorPool::write(size_t index, uint32_t binding, VkDescriptorType type, uint32_t elem, const std::vector<VkDescriptorImageInfo>& images) const {
	return *this;
}

const DescriptorPool& DescriptorPool::write(size_t index, uint32_t binding, VkDescriptorType type, uint32_t elem, const std::vector<VkBufferView>& views) const {
	return *this;
}

const std::vector<VkDescriptorSet>& DescriptorPool::sets() const {
	return sets_;
}