#include <buffer.hpp>
#include <application.hpp>
#include <algorithm>

Buffer::Buffer():buffer_{VK_NULL_HANDLE} {

}

Buffer::Buffer(VkBufferUsageFlags usage, VkDeviceSize size, VkSharingMode mode):buffer_{VK_NULL_HANDLE} {
	VkBufferCreateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	info.usage = usage;
	info.size = size;
	info.sharingMode = mode;
	if (vkCreateBuffer(Application::device().handle(), &info, nullptr, &buffer_) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to create buffer");
	}
}

Buffer::Buffer(Buffer&& other) {
	buffer_ = other.buffer_;
	other.buffer_ = VK_NULL_HANDLE;
}

Buffer::~Buffer() {
	if (buffer_ != VK_NULL_HANDLE) {
		vkDestroyBuffer(Application::device().handle(), buffer_, nullptr);
		buffer_ = VK_NULL_HANDLE;
	}
}

Buffer& Buffer::operator=(Buffer&& other) {
	if (buffer_ != VK_NULL_HANDLE) {
		vkDestroyBuffer(Application::device().handle(), buffer_, nullptr);
	}
	buffer_ = other.buffer_;
	other.buffer_ = VK_NULL_HANDLE;
	return *this;
}

VkBuffer Buffer::handle() const {
	return buffer_;
}

VkMemoryRequirements Buffer::memory_requirements() const {
	VkMemoryRequirements result;
	vkGetBufferMemoryRequirements(Application::device().handle(), buffer_, &result);
	return result;
}

uint32_t memory_type_index(uint32_t type, VkMemoryPropertyFlags flags) {
	auto properties = Application::device().memory_properties();
	for (uint32_t i = 0; i < properties.memoryTypeCount; ++i) {
		if (type & (1 << i) && (properties.memoryTypes[i].propertyFlags & flags) == flags) {
			return i;
		}
	}
	throw std::runtime_error("Error: Failed to find appropriate memory type");
}

DeviceMemory::DeviceMemory():memory_{VK_NULL_HANDLE} {
	
}

DeviceMemory::DeviceMemory(const std::vector<Buffer>& buffers, VkMemoryPropertyFlags flags)
		:memory_{VK_NULL_HANDLE} {

	uint32_t type = buffers.front().memory_requirements().memoryTypeBits;
	for (auto it = buffers.begin(); it != buffers.end(); ++it) {
		auto requirements = it->memory_requirements();
		if ((type & requirements.memoryTypeBits) != type) {
			throw std::runtime_error("Error: Incompatible memory type requirements");
		}
	}

	uint32_t index = memory_type_index(type, flags);
	VkDeviceSize size = 0, offset = 0;
	for (auto it = buffers.begin(); it != buffers.end(); ++it) {
		auto requirements = it->memory_requirements();
		offset = size % requirements.alignment;
		if (offset) {
			offset = requirements.alignment - offset;
		}
		size += offset;
		offsets_.push_back(size);
		size += requirements.size;
	}

	VkMemoryAllocateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	info.allocationSize = size;
	info.memoryTypeIndex = index;
	if (vkAllocateMemory(Application::device().handle(), &info, nullptr, &memory_) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to allocate memory");
	}

	for (size_t i = 0; i < buffers.size(); ++i) {
		vkBindBufferMemory(Application::device().handle(), buffers[i].handle(), memory_, offsets_[i]);
	}
}

DeviceMemory::~DeviceMemory() {
	buffers_.clear();
	if (memory_ != VK_NULL_HANDLE) {
		vkFreeMemory(Application::device().handle(), memory_, nullptr);
		memory_ = VK_NULL_HANDLE;
	}
}

DeviceMemory& DeviceMemory::add_buffer(VkBufferUsageFlags usage, VkDeviceSize sz, VkSharingMode mode) {
	buffers_.push_back(Buffer(usage, sz, mode));
	return *this;
}

DeviceMemory& DeviceMemory::allocate(VkMemoryPropertyFlags flags) {
	if (buffers_.empty()) {
		throw std::runtime_error("Error: No buffers to allocate");
	}

	uint32_t type = buffers_.front().memory_requirements().memoryTypeBits;
	for (auto it = buffers_.begin(); it != buffers_.end(); ++it) {
		auto requirements = it->memory_requirements();
		if ((type & requirements.memoryTypeBits) != type) {
			throw std::runtime_error("Error: Incompatible memory type requirements");
		}
	}

	uint32_t index = memory_type_index(type, flags);
	VkDeviceSize size = 0, offset = 0;
	for (auto it = buffers_.begin(); it != buffers_.end(); ++it) {
		auto requirements = it->memory_requirements();
		offset = size % requirements.alignment;
		if (offset) {
			offset = requirements.alignment - offset;
		}
		size += offset;
		offsets_.push_back(size);
		size += requirements.size;
	}

	VkMemoryAllocateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	info.allocationSize = size;
	info.memoryTypeIndex = index;
	if (vkAllocateMemory(Application::device().handle(), &info, nullptr, &memory_) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to allocate memory");
	}

	for (size_t i = 0; i < buffers_.size(); ++i) {
		vkBindBufferMemory(Application::device().handle(), buffers_[i].handle(), memory_, offsets_[i]);
	}

	return *this;
}

const std::vector<VkDeviceSize>& DeviceMemory::buffer_offsets() const {
	return offsets_;
}

DeviceMemory& DeviceMemory::map(void* data, VkDeviceSize offset, VkDeviceSize size) {
	void *mapped;
	VkDevice device = Application::device().handle();
	vkMapMemory(device, memory_, offset, size, 0, &mapped);
	std::memcpy(mapped, data, static_cast<size_t>(size));
	vkUnmapMemory(device, memory_);
	return *this;
}

std::vector<VkBuffer> DeviceMemory::buffers() const {
	std::vector<VkBuffer> result(buffers_.size());
	std::transform(buffers_.begin(), buffers_.end(), result.begin(), 
		[](const Buffer& buff)->VkBuffer{ return buff.handle(); });
	return result;
}