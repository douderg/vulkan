#include <surface.hpp>
#include <application.hpp>

Surface::Surface():surface_{VK_NULL_HANDLE} {
	VkXcbSurfaceCreateInfoKHR info = {};
	info.sType = VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR;
	info.pNext = nullptr;
	info.connection = Application::window().connection();
	info.window = Application::window().handle();

	if (vkCreateXcbSurfaceKHR(Application::instance().handle(), &info, nullptr, &surface_) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to create surface");
	}
}

Surface::~Surface() {
	if (surface_ != VK_NULL_HANDLE) {
		vkDestroySurfaceKHR(Application::instance().handle(), surface_, nullptr);
		surface_ = VK_NULL_HANDLE;	
	}
}

std::vector<VkSurfaceFormatKHR> Surface::supported_formats(VkPhysicalDevice device) const {
	uint32_t count;
	std::vector<VkSurfaceFormatKHR> result;
	if (vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface_, &count, nullptr) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to get surface formats");
	}
	result.resize(count);
	if (vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface_, &count, result.data()) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to get surface formats");
	}
	return result;
}

std::vector<VkPresentModeKHR> Surface::present_modes(VkPhysicalDevice device) const {
	uint32_t count;
	std::vector<VkPresentModeKHR> result;
	if (vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface_, &count, nullptr) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to get present modes");
	}
	result.resize(count);
	if (vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface_, &count, result.data()) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to get present modes");
	}
	return result;
}

VkSurfaceCapabilitiesKHR Surface::capabilities(VkPhysicalDevice device) const {
	VkSurfaceCapabilitiesKHR result;
	if (vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface_, &result) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to get surface capabilities");
	}
    return result;
}

VkSurfaceKHR Surface::handle() const {
	return surface_;
}

Surface::operator bool() const {
	return surface_ != VK_NULL_HANDLE;
}