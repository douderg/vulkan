#include <swapchain.hpp>
#include <application.hpp>

VkSurfaceFormatKHR get_image_format() {
	auto image_formats = Application::device().supported_formats();
	VkSurfaceFormatKHR result;
	if (image_formats.size() == 1 && image_formats.front().format == VK_FORMAT_UNDEFINED) {
		return { VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
	} 
	auto it = std::find_if(image_formats.begin(), image_formats.end(),
		[](VkSurfaceFormatKHR fmt)->bool { return fmt.format == VK_FORMAT_B8G8R8A8_UNORM && 
				fmt.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR; } );
	return (it != image_formats.end())? *it : image_formats.front();
}

VkExtent2D get_extent(VkSurfaceCapabilitiesKHR capabilities) {
	VkExtent2D result;
	result.width = Application::window().width();
	result.width = std::max(capabilities.minImageExtent.width, result.width);
	result.width = std::min(capabilities.maxImageExtent.width, result.width);
	result.height = Application::window().height();
	result.height = std::max(capabilities.minImageExtent.height, result.height);
	result.height = std::min(capabilities.maxImageExtent.height, result.height);
	return result;
}

Swapchain::Swapchain():swapchain_{VK_NULL_HANDLE} {
	auto present_modes = Application::device().present_modes();
	auto surface_capabilities = Application::device().surface_capabilities();
	image_format_ = get_image_format();
	extent_ = get_extent(surface_capabilities);
	auto queue_indexes = Application::device().queue_indexes();

	VkSwapchainCreateInfoKHR info = {};
	info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	info.surface = Application::device().surface().handle();
	info.minImageCount = surface_capabilities.minImageCount	+ 1;
	info.imageFormat = image_format_.format;
	info.imageColorSpace = image_format_.colorSpace;
	info.imageExtent = extent_;
	info.imageArrayLayers = 1;
	info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	info.queueFamilyIndexCount = queue_indexes.size();
	info.pQueueFamilyIndices = queue_indexes.data();
	info.preTransform = surface_capabilities.currentTransform;
	info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	info.presentMode = VK_PRESENT_MODE_MAILBOX_KHR;
	info.clipped = true;
	info.oldSwapchain = VK_NULL_HANDLE;
	if (vkCreateSwapchainKHR(Application::device().handle(), &info, nullptr, &swapchain_) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to create swapchain");
	}
}

Swapchain::~Swapchain() {
	if (swapchain_ != VK_NULL_HANDLE) {
		vkDestroySwapchainKHR(Application::device().handle(), swapchain_, nullptr);
		swapchain_ = VK_NULL_HANDLE;
	}
}

std::vector<VkImage> Swapchain::images() const {
	uint32_t count;
	if (vkGetSwapchainImagesKHR(Application::device().handle(), swapchain_, &count, nullptr) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to get swapchain images");
	}
	std::vector<VkImage> result;
	if (!count) {
		return result;
	}
	result.resize(count);
	if (vkGetSwapchainImagesKHR(Application::device().handle(), swapchain_, &count, result.data()) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to get swapchain images");
	}
	return result;
}

VkSurfaceFormatKHR Swapchain::image_format() const {
	return image_format_;
}

VkSwapchainKHR Swapchain::handle() const {
	return swapchain_;
}

Swapchain::operator bool() const {
	return swapchain_ != VK_NULL_HANDLE;
}

VkExtent2D Swapchain::extent() const {
	return extent_;
}