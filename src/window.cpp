#include <window.hpp>
#include <cstdlib>
#include <xcb/xcb_ewmh.h>
#include <xcb/xcb_icccm.h>
#include <stdexcept>
#include <cstring>
#include <iostream>

Window::Window() {
	connection_ = xcb_connect(nullptr, nullptr);
	screen_ = xcb_setup_roots_iterator(xcb_get_setup(connection_)).data;
	window_ = xcb_generate_id(connection_);
	// width_ = screen_->width_in_pixels;
	// height_ = screen_->height_in_pixels;
	width_ = 640;
	height_ = 480;
	uint32_t options[] = {
		screen_->black_pixel,
		XCB_EVENT_MASK_EXPOSURE |
		XCB_EVENT_MASK_BUTTON_PRESS |
		XCB_EVENT_MASK_BUTTON_RELEASE |
		XCB_EVENT_MASK_KEY_PRESS |
		XCB_EVENT_MASK_KEY_RELEASE |
		XCB_EVENT_MASK_POINTER_MOTION |
		XCB_EVENT_MASK_BUTTON_MOTION |
		XCB_EVENT_MASK_STRUCTURE_NOTIFY
	};

	xcb_create_window(
		connection_,
		XCB_COPY_FROM_PARENT, // depth
		window_, // id
		screen_->root, // parent id 
		0, // top left X
		0, // top left Y
		width_, // width
		height_, // height
		10, // border
		XCB_WINDOW_CLASS_INPUT_OUTPUT, 
		screen_->root_visual, 
		XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK, 
		options);
	xcb_ewmh_connection_t ewmh;
	xcb_intern_atom_cookie_t *ewmh_cookie = xcb_ewmh_init_atoms(connection_, &ewmh);
	if (!xcb_ewmh_init_atoms_replies(&ewmh, ewmh_cookie, nullptr)) {
		throw std::runtime_error("Could not retrieve atom from WM");
	}
	// xcb_change_property(connection_, 
	// 	XCB_PROP_MODE_REPLACE,
	// 	window_,
	// 	ewmh._NET_WM_STATE,
	// 	XCB_ATOM_ATOM,
	// 	32,
	// 	1,
	// 	&ewmh._NET_WM_STATE_FULLSCREEN);
	// xcb_ewmh_connection_wipe(&ewmh);
	xcb_map_window(connection_, window_);
	xcb_flush(connection_);
	
	xcb_generic_event_t *e;
    while ((e = xcb_wait_for_event(connection_))) {
        if ((e->response_type & ~0x80) == XCB_EXPOSE) {
            break;
        }
        free(e);
    }
    free(e);
}

Window::~Window() {
	xcb_disconnect(connection_);
	connection_ = nullptr;
}

std::vector<Window::Event> Window::events() {
	xcb_generic_event_t *e;
	std::vector<Event> result;
	while ((e = xcb_poll_for_event(connection_))) {
		switch (e->response_type & ~0x80) {
			case XCB_EXPOSE:
				result.push_back(EXPOSE);
				break;
			case XCB_BUTTON_PRESS:
				result.push_back(MOUSE_DOWN);
				break;
				// ((xcb_button_press_event_t*) e)
			case XCB_BUTTON_RELEASE:
				result.push_back(MOUSE_UP);
				break;
			case XCB_KEY_PRESS:
				result.push_back(KEY_DOWN);
				break; 
				//on_key_down(((xcb_key_press_event_t*)e)->detail); break;
			case XCB_KEY_RELEASE:
				result.push_back(KEY_UP);
				break;
			case XCB_MOTION_NOTIFY: 
				result.push_back(MOUSE_MOVE);
				break;
			case XCB_CONFIGURE_NOTIFY:
				width_ = ((xcb_configure_notify_event_t*)e)->width > 0?((xcb_configure_notify_event_t*) e)->width : width_;
				height_ = ((xcb_configure_notify_event_t*)e)->height > 0?((xcb_configure_notify_event_t*) e)->height : height_;
				result.push_back(EXPOSE);
				break;
			default:
				break;
		}
		free(e);
	}
	return result;
}

xcb_connection_t* Window::connection() const {
	return connection_;
}

xcb_window_t Window::handle() const {
	return window_;
}

uint32_t Window::width() const {
	return width_;
}

uint32_t Window::height() const {
	return height_;
}

void Window::set_title(const std::string& title) {
	xcb_change_property(connection_,
		XCB_PROP_MODE_REPLACE,
		window_,
		XCB_ATOM_WM_NAME,
		XCB_ATOM_STRING,
		8,
		std::strlen(title.c_str()),
		title.c_str());
	xcb_flush(connection_); 
}

void Window::set_extent_limits(uint32_t min_width, uint32_t min_height, uint32_t max_width, uint32_t max_height) {
	xcb_size_hints_t hints;
    xcb_icccm_size_hints_set_min_size(&hints, min_width, min_height);
    xcb_icccm_size_hints_set_max_size(&hints, max_width, max_height);
    xcb_icccm_set_wm_size_hints(connection_, window_, XCB_ATOM_WM_NORMAL_HINTS, &hints);
}
