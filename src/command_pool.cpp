#include <command_pool.hpp>
#include <application.hpp>

CommandPool::CommandPool():pool_{VK_NULL_HANDLE} {
	VkCommandPoolCreateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	info.queueFamilyIndex = Application::device().queue_indexes().front();
	if (vkCreateCommandPool(Application::device().handle(), &info, nullptr, &pool_) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to create command pool");
	}
}

CommandPool::CommandPool(VkCommandPoolCreateFlags flags):pool_{VK_NULL_HANDLE} {
	VkCommandPoolCreateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	info.queueFamilyIndex = Application::device().queue_indexes().front();
	info.flags = flags;
	if (vkCreateCommandPool(Application::device().handle(), &info, nullptr, &pool_) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to create command pool");
	}
}

CommandPool::~CommandPool() {
	if (pool_ != VK_NULL_HANDLE) {
		vkDestroyCommandPool(Application::device().handle(), pool_, nullptr);
		buffers_.clear();
		pool_ = VK_NULL_HANDLE;
	}
}

VkCommandPool CommandPool::handle() const {
	return pool_;
}

CommandPool::operator bool() const {
	return pool_ != VK_NULL_HANDLE;
}

void CommandPool::allocate_buffers(size_t size) {
	if (pool_ != VK_NULL_HANDLE && !buffers_.empty()) {
		vkFreeCommandBuffers(Application::device().handle(), pool_, buffers_.size(), buffers_.data());
		buffers_.clear();
	}
	buffers_.resize(size, VK_NULL_HANDLE);
	VkCommandBufferAllocateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	info.commandPool = pool_;
	info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	info.commandBufferCount = size;
	if (vkAllocateCommandBuffers(Application::device().handle(), &info, buffers_.data()) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to allocate command buffers");
	}
}

std::vector<VkCommandBuffer> CommandPool::buffers() const {
	return buffers_;
}

VkCommandBuffer CommandPool::buffer(size_t index) const {
	return buffers_.at(index);
}

void CommandPool::record_commands(std::function<void(VkCommandBuffer)> commands) const {
	record_commands(VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT, commands);
}

void CommandPool::record_commands(VkCommandBufferUsageFlags flags, std::function<void(VkCommandBuffer)> commands) const {
	VkExtent2D extent = Application::swapchain().extent();
	Application::pipeline().render_pass().clear_values({{0.0f, 0.0f, 0.0f, 1.0f}});
	for (size_t i = 0; i < buffers_.size(); ++i) {
		VkCommandBuffer cmd_buff = buffers_[i];
		VkCommandBufferBeginInfo begin_info = {};
		begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		begin_info.flags = flags;
		if (vkBeginCommandBuffer(cmd_buff, &begin_info) != VK_SUCCESS) {
			throw std::runtime_error("Error: Failed to start recording command buffer");
		}
		Application::pipeline()
			.render_pass()
			.target(Application::frame_buffers()[i].handle(), {0, 0}, extent)
			.commands(cmd_buff)
			.begin();
		commands(cmd_buff);
		vkCmdEndRenderPass(cmd_buff);
		if (vkEndCommandBuffer(cmd_buff) != VK_SUCCESS) {
			throw std::runtime_error("Error: Failed to record command buffer");
		}
	}
}

void CommandPool::record(VkCommandBufferUsageFlags flags, std::function<void(VkCommandBuffer)> func) const {
	for (auto it = buffers_.begin(); it != buffers_.end(); ++it) {
		VkCommandBufferBeginInfo begin_info = {};
		begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		begin_info.flags = flags;
		if (vkBeginCommandBuffer(*it, &begin_info) != VK_SUCCESS) {
			throw std::runtime_error("Error: Failed to start recording command buffer");
		}
		func(*it);
		if (vkEndCommandBuffer(*it) != VK_SUCCESS) {
			throw std::runtime_error("Error: Failed to record command buffer");
		}
	}
}

void CommandPool::record(VkCommandBufferUsageFlags flags, std::function<void(VkCommandBuffer, int)> func) const {
	for (auto it = buffers_.begin(); it != buffers_.end(); ++it) {
		VkCommandBufferBeginInfo begin_info = {};
		begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		begin_info.flags = flags;
		if (vkBeginCommandBuffer(*it, &begin_info) != VK_SUCCESS) {
			throw std::runtime_error("Error: Failed to start recording command buffer");
		}
		func(*it, it - buffers_.begin());
		if (vkEndCommandBuffer(*it) != VK_SUCCESS) {
			throw std::runtime_error("Error: Failed to record command buffer");
		}
	}
}