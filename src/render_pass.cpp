#include <render_pass.hpp>
#include <application.hpp>

RenderPass::RenderPass():render_pass_{VK_NULL_HANDLE} {
	std::vector<VkAttachmentDescription> attachments;
	VkAttachmentDescription desc = {};
	desc.format = Application::swapchain().image_format().format;
	desc.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	desc.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	desc.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	desc.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	desc.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	desc.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
	attachments.push_back(desc);

	VkAttachmentReference attachment_ref = {};
	attachment_ref.attachment = 0;
	attachment_ref.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	VkSubpassDescription subpass = {};
	subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpass.colorAttachmentCount = 1;
	subpass.pColorAttachments = &attachment_ref;

	VkSubpassDependency dependency = {};
	dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
	dependency.dstSubpass = 0;
	dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.srcAccessMask = 0;
	dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

	VkRenderPassCreateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	info.attachmentCount = attachments.size();
	info.pAttachments = attachments.data();
	info.subpassCount = 1;
	info.pSubpasses = &subpass;
	info.dependencyCount = 1;
	info.pDependencies = &dependency;

	if (vkCreateRenderPass(Application::device().handle(), &info, nullptr, &render_pass_) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to create render pass");
	}
}

RenderPass::~RenderPass() {
	if (render_pass_ != VK_NULL_HANDLE) {
		vkDestroyRenderPass(Application::device().handle(), render_pass_, nullptr);
		render_pass_ = VK_NULL_HANDLE;
	}
}

VkRenderPass RenderPass::handle() const {
	return render_pass_;
}

RenderPass& RenderPass::clear_values(const std::vector<VkClearValue>& values) {
	clear_values_ = values;
	return *this;
}

RenderPass& RenderPass::target(VkFramebuffer framebuffer, VkOffset2D offset, VkExtent2D extent) {
	framebuffer_ = framebuffer;
	offset_ = offset;
	extent_ = extent;
	return *this;
}

RenderPass& RenderPass::commands(VkCommandBuffer cmd_buffer) {
	commands_ = cmd_buffer;
	return *this;
}

void RenderPass::begin() const {
	VkRenderPassBeginInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	info.renderPass = render_pass_;
	info.framebuffer = framebuffer_;
	info.renderArea.offset = offset_;
	info.renderArea.extent = extent_;
	info.clearValueCount = clear_values_.size();
	info.pClearValues = !clear_values_.empty()? clear_values_.data() : nullptr;
	vkCmdBeginRenderPass(commands_, &info, VK_SUBPASS_CONTENTS_INLINE);
}