#include <image_view.hpp>
#include <application.hpp>

ImageView::ImageView():image_{VK_NULL_HANDLE}, view_{VK_NULL_HANDLE} {

}

ImageView::ImageView(VkImage image, VkFormat format):image_{image}, view_{VK_NULL_HANDLE} {
	VkImageViewCreateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	info.image = image;
	info.format = format;
	info.components.a = VK_COMPONENT_SWIZZLE_A;
	info.components.r = VK_COMPONENT_SWIZZLE_R;
	info.components.g = VK_COMPONENT_SWIZZLE_G;
	info.components.b = VK_COMPONENT_SWIZZLE_B;
	info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	info.subresourceRange.levelCount = 1;
	info.subresourceRange.baseMipLevel = 0;
	info.subresourceRange.layerCount = 1;
	info.subresourceRange.baseArrayLayer = 0;
	if (vkCreateImageView(Application::device().handle(), &info, nullptr, &view_) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to create image view");
	}
}

ImageView::~ImageView() {
	if (view_ != VK_NULL_HANDLE) {
		vkDestroyImageView(Application::device().handle(), view_, nullptr);
		view_ = VK_NULL_HANDLE;
	}
}

ImageView::ImageView(ImageView&& other) {
	view_ = other.view_;
	image_ = other.image_;
	other.view_ = VK_NULL_HANDLE;
	other.image_ = VK_NULL_HANDLE;
}

ImageView& ImageView::operator=(ImageView&& other) {
	if (view_ != VK_NULL_HANDLE) {
		vkDestroyImageView(Application::device().handle(), view_, nullptr);
	}
	view_ = other.view_;
	image_ = other.image_;
	other.view_ = VK_NULL_HANDLE;
	other.image_ = VK_NULL_HANDLE;
	return *this;
}

VkImageView ImageView::handle() const {
	return view_;
}