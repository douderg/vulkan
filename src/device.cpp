#include <device.hpp>
#include <application.hpp>
#include <stdexcept>
#include <numeric>

std::vector<VkPhysicalDevice> physical_devices(VkInstance instance) {
	uint32_t count;
	std::vector<VkPhysicalDevice> physical_devices{};
	if (vkEnumeratePhysicalDevices(instance, &count, nullptr) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to detect physical devices");
	}
	if (count > 0) {
		physical_devices.resize(count);
		if (vkEnumeratePhysicalDevices(instance, &count, physical_devices.data()) != VK_SUCCESS) {
			throw std::runtime_error("Error: Failed to detect physical devices");
		}
	}
	return physical_devices;
}

std::vector<VkQueueFamilyProperties> queue_family_properties(VkPhysicalDevice device) {
	uint32_t count;
	std::vector<VkQueueFamilyProperties> result {};
	vkGetPhysicalDeviceQueueFamilyProperties(device, &count, nullptr);
	if (count > 0) {
		result.resize(count);
		vkGetPhysicalDeviceQueueFamilyProperties(device, &count, result.data());
	}
	return result;
}

std::vector<VkExtensionProperties> get_available_extensions(VkPhysicalDevice device) {
	uint32_t count = 0;
	if (vkEnumerateDeviceExtensionProperties(device, nullptr, &count, nullptr) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to get supported extensions");
	}
	std::vector<VkExtensionProperties> result;
	if (count) {
		result.resize(count);
		if (vkEnumerateDeviceExtensionProperties(device, nullptr, &count, result.data()) != VK_SUCCESS) {
			throw std::runtime_error("Error: Failed to get supported extensions");
		}
	}
	return result;
}

class PhysicalDeviceSelector {
public:
	PhysicalDeviceSelector(VkSurfaceKHR surface, const std::vector<const char*>& extensions)
			:surface_{surface}, extensions_{extensions} {

	}

	bool operator()(VkPhysicalDevice device) {
		auto available_extensions = get_available_extensions(device);
		for (auto it = extensions_.begin(); it != extensions_.end(); ++it) {
			auto it2 = std::find_if(available_extensions.begin(), available_extensions.end(),
				[&](VkExtensionProperties prop)->bool{ return std::strcmp(*it, prop.extensionName) == 0; });
			if (it2 == available_extensions.end()) {
				return false;
			}
		}
		
		auto properties = queue_family_properties(device);
		VkBool32 supports_graphics, supports_surface;
		for (size_t i = 0; i < properties.size() && !(supports_graphics && supports_surface); ++i) {
			if (properties[i].queueCount) {
				if (!supports_graphics && properties[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
					graphics_index_ = i;
					supports_graphics = true;
				}
				if (!supports_surface) {
					vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface_, &supports_surface);
					if (supports_surface) {
						surface_index_ = i;
					}
				}
			}
		}
		return supports_graphics && supports_surface;
	}

	uint32_t graphics_queue_index() const {
		return graphics_index_;
	}

	uint32_t surface_queue_index() const {
		return surface_index_;
	}

private:
	VkSurfaceKHR surface_;
	uint32_t graphics_index_, surface_index_;
	std::vector<const char*> extensions_;
};

Device::Device():device_{VK_NULL_HANDLE} {
	auto devices = physical_devices(Application::instance().handle());
	std::vector<const char*> required_extensions {VK_KHR_SWAPCHAIN_EXTENSION_NAME};

	PhysicalDeviceSelector selector(surface_.handle(), required_extensions);
	auto it = std::find_if(devices.begin(), devices.end(), selector);
	if (it == devices.end()) {
		throw std::runtime_error("Error: Failed to find appropriate device");
	}
	physical_device_ = *it;

	// auto cap = surface_capabilities();
	// Application::window().set_extent_limits(cap.minImageExtent.width, cap.minImageExtent.height, 	cap.maxImageExtent.width, cap.maxImageExtent.height);

	std::vector<VkDeviceQueueCreateInfo> queue_create_infos;
	std::vector<float> queue_priorities = { 1.0 };
	VkDeviceQueueCreateInfo graphics_queue_info{};
	graphics_queue_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
	graphics_queue_info.queueFamilyIndex = selector.graphics_queue_index();
	graphics_queue_info.pNext = nullptr;
	graphics_queue_info.pQueuePriorities = queue_priorities.data();
	graphics_queue_info.queueCount = 1;
	queue_create_infos.push_back(graphics_queue_info);
	queue_indexes_.push_back(selector.graphics_queue_index());

	if (selector.graphics_queue_index() != selector.surface_queue_index()) {
		VkDeviceQueueCreateInfo surface_queue_info{};
		surface_queue_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		surface_queue_info.queueFamilyIndex = selector.surface_queue_index();
		surface_queue_info.pNext = nullptr;
		surface_queue_info.pQueuePriorities = queue_priorities.data();
		surface_queue_info.queueCount = 1;
		queue_create_infos.push_back(surface_queue_info);
		queue_indexes_.push_back(selector.surface_queue_index());
	}
	
	VkDeviceCreateInfo device_info {};
	device_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	device_info.queueCreateInfoCount = queue_create_infos.size();
	device_info.pQueueCreateInfos = queue_create_infos.data();
	device_info.pEnabledFeatures = nullptr;
	device_info.enabledExtensionCount = required_extensions.size();
	device_info.ppEnabledExtensionNames = required_extensions.data();
	device_info.enabledLayerCount = 0;
	device_info.ppEnabledLayerNames = nullptr;

	if (vkCreateDevice(physical_device_, &device_info, nullptr, &device_) != VK_SUCCESS) {
		throw std::runtime_error("Failed to create logical device");
	}
	queues_.resize(1);
	if (selector.graphics_queue_index() != selector.surface_queue_index()) {
		queues_.resize(2);
		vkGetDeviceQueue(device_, queue_indexes_.back(), 0, queues_.data() + 1);
	}
	
	vkGetDeviceQueue(device_, queue_indexes_.front(), 0, queues_.data());
	
}

Device::~Device() {
	if (device_ != VK_NULL_HANDLE) {
		vkDestroyDevice(device_, nullptr);
		device_ = VK_NULL_HANDLE;
	}
}

VkDevice Device::handle() const {
	return device_;
}

const Surface& Device::surface() const {
	if (!surface_) {
		throw std::runtime_error("Error: Invalid surface");
	}
	return surface_;
}

std::vector<VkSurfaceFormatKHR> Device::supported_formats() const {
	if (!surface_) {
		throw std::runtime_error("Error: Invalid surface");
	}
	return surface_.supported_formats(physical_device_);
}

std::vector<VkPresentModeKHR> Device::present_modes() const {
	if (!surface_) {
		throw std::runtime_error("Error: Invalid surface");
	}
	return surface_.present_modes(physical_device_);
}

VkSurfaceCapabilitiesKHR Device::surface_capabilities() const {
	if (!surface_) {
		throw std::runtime_error("Error: Invalid surface");
	}
	return surface_.capabilities(physical_device_);
}

VkPhysicalDeviceMemoryProperties Device::memory_properties() const {
	VkPhysicalDeviceMemoryProperties result;
	vkGetPhysicalDeviceMemoryProperties(physical_device_, &result);
	return result;
}

std::vector<uint32_t> Device::queue_indexes() const {
	return queue_indexes_;
}

std::vector<VkQueue> Device::queues() const {
	return queues_;
}

Device::operator bool() const {
	return device_ != VK_NULL_HANDLE;
}

SubmitQueue::SubmitQueue(VkQueue queue):queue_{queue} {

}

SubmitQueue& SubmitQueue::waits(const std::vector<VkSemaphore>& semaphores) {
	wait_ = semaphores;
	return *this;
}

SubmitQueue& SubmitQueue::waits(const std::vector<VkPipelineStageFlags>& stages) {
	stages_ = stages;
	return *this;
}

SubmitQueue& SubmitQueue::fires(const std::vector<VkSemaphore>& semaphores) {
	fire_ = semaphores;
	return *this;
}

SubmitQueue& SubmitQueue::command_buffers(const std::vector<VkCommandBuffer>& buffers) {
	buffers_ = buffers;
	return *this;
}

VkResult SubmitQueue::submit() const {
	return submit(VK_NULL_HANDLE);
}

VkResult SubmitQueue::submit(VkFence fence) const {
	VkSubmitInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	info.waitSemaphoreCount = wait_.size();
	info.pWaitSemaphores = !wait_.empty()? wait_.data() : nullptr;
	info.pWaitDstStageMask = !stages_.empty()? stages_.data() : nullptr;
	info.signalSemaphoreCount = fire_.size();
	info.pSignalSemaphores = !fire_.empty()? fire_.data() : nullptr;
	info.commandBufferCount = buffers_.size();
	info.pCommandBuffers = !buffers_.empty()? buffers_.data() : nullptr;
	return vkQueueSubmit(queue_, 1, &info, fence);
}

PresentQueue::PresentQueue(VkQueue queue):queue_{queue} {

}

PresentQueue& PresentQueue::waits(const std::vector<VkSemaphore>& semaphores) {
	wait_ = semaphores;
	return *this;
}

PresentQueue& PresentQueue::swapchains(const std::vector<VkSwapchainKHR>& chains) {
	swapchain_ = chains;
	return *this;
}

PresentQueue& PresentQueue::images(const std::vector<uint32_t>& images) {
	images_ = images;
	return *this;
}

VkResult PresentQueue::present() const {
	VkPresentInfoKHR info = {};
	info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	info.waitSemaphoreCount = wait_.size();
	info.pWaitSemaphores = !wait_.empty()? wait_.data() : nullptr;
	info.swapchainCount = swapchain_.size();
	info.pSwapchains = !swapchain_.empty()? swapchain_.data() : nullptr;
	info.pImageIndices = !images_.empty()? images_.data() : nullptr;
	return vkQueuePresentKHR(queue_, &info);
}


Semaphore::Semaphore():semaphore_{VK_NULL_HANDLE} {
	VkSemaphoreCreateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	if (vkCreateSemaphore(Application::device().handle(), &info, nullptr, &semaphore_) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to create semaphore");
	}
}

Semaphore::~Semaphore() {
	if (semaphore_ != VK_NULL_HANDLE) {
		vkDestroySemaphore(Application::device().handle(), semaphore_, nullptr);
		semaphore_ = VK_NULL_HANDLE;
	}
}

VkSemaphore Semaphore::handle() const {
	return semaphore_;
}

Fence::Fence():fence_{VK_NULL_HANDLE} {
	VkFenceCreateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	info.flags = VK_FENCE_CREATE_SIGNALED_BIT;
	if (vkCreateFence(Application::device().handle(), &info, nullptr, &fence_) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to create fence");
	}
}

Fence::~Fence() {
	if (fence_ != VK_NULL_HANDLE) {
		vkDestroyFence(Application::device().handle(), fence_, nullptr);
		fence_ = VK_NULL_HANDLE;
	}
}

VkFence Fence::handle() const {
	return fence_;
}

void Fence::wait() const {
	VkResult res = vkWaitForFences(Application::device().handle(), 1, &fence_, VK_TRUE, std::numeric_limits<uint64_t>::max());
	switch (res) {
		case VK_SUCCESS:
		case VK_TIMEOUT:
			vkResetFences(Application::device().handle(), 1, &fence_);
			break;
		default:
			throw std::runtime_error("Error: Failure while waiting for fence");
	}
}