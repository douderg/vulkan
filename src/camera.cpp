#include <camera.hpp>
#include <glm/gtc/matrix_transform.hpp>


Camera::Camera(): mvp_{}, start_{std::chrono::high_resolution_clock().now()} {
	mvp_.m = glm::mat4(1.0f);
	mvp_.p = glm::mat4(1.0f);
	mvp_.v = glm::mat4(1.0f);
}

Camera::~Camera() {

}

MVPMatrix* Camera::ubo() {
	return &mvp_;
}

Camera& Camera::perspective(float fovy, float aspect, float znear, float zfar) {
	mvp_.p = glm::perspective(fovy, aspect, znear, zfar);
	return *this;
}

Camera& Camera::look_at(glm::vec3 pos, glm::vec3 direction, glm::vec3 up) {
	mvp_.v = glm::lookAt(pos, direction, up);
	return *this;
}

Camera& Camera::update() {
	auto x = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock().now() - start_).count();
	mvp_.m = glm::rotate(glm::mat4(1.0f), x * glm::radians(90.0f) / 1000.0f, glm::vec3(0.0f, 0.0f, 1.0f));
	return *this;
}