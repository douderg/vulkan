#include <framebuffer.hpp>
#include <application.hpp>

Framebuffer::Framebuffer():buffer_{VK_NULL_HANDLE} {

}

Framebuffer::Framebuffer(VkRenderPass pass, const std::vector<VkImageView>& views, VkExtent2D extent)
		:buffer_{VK_NULL_HANDLE} {
	VkFramebufferCreateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
	info.renderPass = pass;
	info.attachmentCount = views.size();
	info.pAttachments = views.data();
	info.width = extent.width;
	info.height = extent.height;
	info.layers = 1;
	if (vkCreateFramebuffer(Application::device().handle(), &info, nullptr, &buffer_) != VK_SUCCESS) {
		throw std::runtime_error("Error: Failed to create framebuffer");
	}
}

Framebuffer::~Framebuffer() {
	if (buffer_ != VK_NULL_HANDLE) {
		vkDestroyFramebuffer(Application::device().handle(), buffer_, nullptr);
		buffer_ = VK_NULL_HANDLE;
	}
}

Framebuffer::Framebuffer(Framebuffer&& other) {
	buffer_ = other.buffer_;
	other.buffer_ = VK_NULL_HANDLE;
}

Framebuffer& Framebuffer::operator=(Framebuffer&& other) {
	if (buffer_ != VK_NULL_HANDLE) {
		vkDestroyFramebuffer(Application::device().handle(), buffer_, nullptr);
	}
	buffer_ = other.buffer_;
	other.buffer_ = VK_NULL_HANDLE;
	return *this;
}

VkFramebuffer Framebuffer::handle() const {
	return buffer_;
}