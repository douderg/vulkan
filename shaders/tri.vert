#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 color;

layout(binding = 0) uniform MVPMatrix {
	mat4 m;
	mat4 v;
	mat4 p;
} mvp;

layout(location = 0) out vec3 fragColor;

void main() {
    gl_Position = mvp.p * mvp.v * mvp.m * vec4(position, 1.0);
    fragColor = color;
}