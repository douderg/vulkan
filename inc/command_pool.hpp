#ifndef _COMMAND_POOL_HPP_
#define _COMMAND_POOL_HPP_

#include <vulkan/vulkan.hpp>
#include <vector>
#include <functional>

class CommandPool {
public:
	CommandPool();
	CommandPool(VkCommandPoolCreateFlags);
	~CommandPool();
	VkCommandPool handle() const;
	operator bool() const;
	void allocate_buffers(size_t);
	std::vector<VkCommandBuffer> buffers() const;
	VkCommandBuffer buffer(size_t) const;
	void record_commands(std::function<void(VkCommandBuffer)>) const;
	void record_commands(VkCommandBufferUsageFlags, std::function<void(VkCommandBuffer)>) const;
	void record(VkCommandBufferUsageFlags, std::function<void(VkCommandBuffer)>) const;
	void record(VkCommandBufferUsageFlags, std::function<void(VkCommandBuffer, int)>) const;
private:
	VkCommandPool pool_;
	std::vector<VkCommandBuffer> buffers_;
};

#endif