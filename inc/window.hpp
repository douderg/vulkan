#ifndef _WINDOW_HPP_
#define _WINDOW_HPP_

#include <vector>
#include <string>
#include <xcb/xcb.h>

class Window {
public:
	enum Event {
		KEY_DOWN,
		KEY_UP,
		MOUSE_DOWN,
		MOUSE_UP,
		MOUSE_MOVE,
		EXPOSE
	};
	Window();
	~Window();
	std::vector<Event> events();
	xcb_connection_t* connection() const;
	xcb_window_t handle() const;
	uint32_t width() const;
	uint32_t height() const;
	void set_title(const std::string&);
	void set_extent_limits(uint32_t, uint32_t, uint32_t, uint32_t);
private:
	xcb_connection_t *connection_;
	xcb_screen_t *screen_;
	xcb_window_t window_;
	uint32_t width_, height_;
};

#endif