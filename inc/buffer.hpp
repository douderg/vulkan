#ifndef _BUFFER_HPP_
#define _BUFFER_HPP_

#include <vulkan/vulkan.hpp>
#include <initializer_list>
#include <vector>

class Buffer {
public:
	Buffer();
	Buffer(VkBufferUsageFlags, VkDeviceSize, VkSharingMode);
	Buffer(const Buffer&) = delete;
	Buffer(Buffer&&);
	~Buffer();
	Buffer& operator=(const Buffer&) = delete;
	Buffer& operator=(Buffer&&);
	VkBuffer handle() const;
	VkMemoryRequirements memory_requirements() const;
private:
	VkBuffer buffer_;
};

class DeviceMemory {
public:
	DeviceMemory();
	DeviceMemory(const std::vector<Buffer>&, VkMemoryPropertyFlags);
	~DeviceMemory();
	DeviceMemory& add_buffer(VkBufferUsageFlags, VkDeviceSize, VkSharingMode);
	DeviceMemory& allocate(VkMemoryPropertyFlags);
	const std::vector<VkDeviceSize>& buffer_offsets() const;
	std::vector<VkBuffer> buffers() const;
	DeviceMemory& map(void*, VkDeviceSize, VkDeviceSize);
private:
	VkDeviceMemory memory_;
	std::vector<VkDeviceSize> offsets_;
	std::vector<Buffer> buffers_;
};

#endif