#ifndef _FRAMEBUFFER_HPP_
#define _FRAMEBUFFER_HPP_

#include <vulkan/vulkan.hpp>
#include <vector>

class Framebuffer {
public:
	Framebuffer();
	Framebuffer(VkRenderPass, const std::vector<VkImageView>&, VkExtent2D);
	~Framebuffer();
	Framebuffer(const Framebuffer&) = delete;
	Framebuffer(Framebuffer&&);
	Framebuffer& operator=(const Framebuffer&) = delete;
	Framebuffer& operator=(Framebuffer&&);
	VkFramebuffer handle() const;
private:
	VkFramebuffer buffer_;
};

#endif