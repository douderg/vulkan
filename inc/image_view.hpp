#ifndef _IMAGE_VIEW_HPP_
#define _IMAGE_VIEW_HPP_

#include <vulkan/vulkan.hpp>
#include <swapchain.hpp>

class ImageView {
public:
	ImageView();
	ImageView(VkImage, VkFormat);
	ImageView(const ImageView&) = delete;
	ImageView(ImageView&&);
	ImageView& operator=(const ImageView&) = delete;
	ImageView& operator=(ImageView&&);
	~ImageView();
	VkImageView handle() const;
private:
	VkImage image_;
	VkImageView view_;
};

#endif