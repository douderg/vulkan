#ifndef _APPLICATION_HPP_
#define _APPLICATION_HPP_

#include <vulkan/vulkan.hpp>
#include <memory>
#include <device.hpp>
#include <buffer.hpp>
#include <window.hpp>
#include <swapchain.hpp>
#include <image_view.hpp>
#include <command_pool.hpp>
#include <pipeline.hpp>
#include <framebuffer.hpp>
#include <camera.hpp>

class VulkanInstance {
public:
	VulkanInstance();
	~VulkanInstance();
	VkInstance handle() const;
	operator bool() const;
private:
	VkInstance instance_;
};

class Application {
public:
	static int run(int, const char**);
	static const VulkanInstance& instance();
	static Window& window();
	static const Device& device();
	static const DeviceMemory& memory();
	static const Swapchain& swapchain();
	static Pipeline& pipeline();
	static const CommandPool& command_pool();
	static const std::vector<Framebuffer>& frame_buffers();
	~Application();
private:
	Application(int, const char**);

	void update_swapchain();
	void render_loop();

	static std::unique_ptr<Application> instance_;
	VulkanInstance vulkan_instance_;
	Window window_;
	Camera camera_;
	bool running_;
	std::unique_ptr<Device> device_;
	std::unique_ptr<DeviceMemory> gpu_memory_;
	std::unique_ptr<DeviceMemory> ubos_;
	std::unique_ptr<CommandPool> cmd_pool_;
	std::unique_ptr<Swapchain> swapchain_;
	std::vector<ImageView> views_;
	std::vector<DescriptorSetLayout> layouts_;
	std::unique_ptr<Pipeline> pipeline_;
	std::unique_ptr<DescriptorPool> descriptor_pool_;
	std::vector<Framebuffer> framebuffers_;
};

#endif