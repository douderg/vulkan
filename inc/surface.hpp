#ifndef _SURFACE_HPP_
#define _SURFACE_HPP_

#include <vulkan/vulkan.hpp>
#include <xcb/xcb.h>
#include <vulkan/vulkan_xcb.h>
#include <vector>

class Surface {
public:
	Surface();
	~Surface();
	std::vector<VkSurfaceFormatKHR> supported_formats(VkPhysicalDevice) const;
	std::vector<VkPresentModeKHR> present_modes(VkPhysicalDevice) const;
	VkSurfaceCapabilitiesKHR capabilities(VkPhysicalDevice) const;
	VkSurfaceKHR handle() const;
	operator bool() const;
private:
	VkSurfaceKHR surface_;
};

#endif