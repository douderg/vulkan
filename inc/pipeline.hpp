#ifndef _PIPELINE_HPP_
#define _PIPELINE_HPP_

#include <vulkan/vulkan.hpp>
#include <render_pass.hpp>

class ShaderStage {
public:
	ShaderStage(VkShaderStageFlagBits, const std::string &);
	VkShaderModuleCreateInfo info();
	VkShaderStageFlagBits flags() const;
private:
	VkShaderStageFlagBits _flags;
	std::vector<char> _code;
};

class ShaderBuild {
public:
	ShaderBuild(const std::vector<ShaderStage> &);
	ShaderBuild(const ShaderBuild &) = delete;
	ShaderBuild &operator=(const ShaderBuild &) = delete;
	~ShaderBuild();
	std::vector<VkPipelineShaderStageCreateInfo> info() const;
private:
	std::vector<std::pair<VkShaderStageFlagBits, VkShaderModule>> _modules;
};

class VertexInputState {
public:
	VertexInputState& add_binding(uint32_t, uint32_t, VkVertexInputRate);
	VertexInputState& add_attribute(uint32_t, uint32_t, const VkFormat &, uint32_t);
	VkPipelineVertexInputStateCreateInfo info() const;
private:
	std::vector<VkVertexInputBindingDescription> _bindings;
	std::vector<VkVertexInputAttributeDescription> _attributes;
};

class InputAssembly {
public:
	VkPipelineInputAssemblyStateCreateInfo info() const;
private:
};

class ViewportState {
public:
	ViewportState();
	ViewportState(const VkExtent2D &);
	VkPipelineViewportStateCreateInfo info() const;
private:
	VkViewport _viewport;
	VkRect2D _scissor;
};

class RasterizationState {
public:
	VkPipelineRasterizationStateCreateInfo info() const;
private:
};

class MultisampleState {
public:
	VkPipelineMultisampleStateCreateInfo info() const;
private:
};

class DepthStencilState {
public:
	VkPipelineDepthStencilStateCreateInfo info() const;
private:
};

class ColorBlendState {
public:
	ColorBlendState();
	VkPipelineColorBlendStateCreateInfo info() const;
private:
	VkPipelineColorBlendAttachmentState attachment_;
};

class DynamicState {
public:
	DynamicState();
	DynamicState(const std::vector<VkDynamicState> &);
	VkPipelineDynamicStateCreateInfo info() const;
private:
	std::vector<VkDynamicState> _states;
};

class PipelineLayout
{
public:
	PipelineLayout(const std::vector<VkDescriptorSetLayout>& layouts);
	~PipelineLayout();
	VkPipelineLayout handle() const;
private:
	VkPipelineLayout layout_;
};

class DescriptorSetLayout {
public:
	DescriptorSetLayout();
	~DescriptorSetLayout();
	DescriptorSetLayout(const DescriptorSetLayout&) = delete;
	DescriptorSetLayout(DescriptorSetLayout&&);
	DescriptorSetLayout& add_binding(uint32_t, VkDescriptorType, uint32_t, VkShaderStageFlagBits);
	DescriptorSetLayout& add_binding(uint32_t, VkDescriptorType, uint32_t, VkShaderStageFlagBits, const VkSampler*);
	DescriptorSetLayout& create();
	VkDescriptorSetLayout handle() const;
private:
	VkDescriptorSetLayout layout_;
	std::vector<VkDescriptorSetLayoutBinding> bindings_;
};

VkDescriptorBufferInfo buffer_write(VkBuffer, VkDeviceSize, VkDeviceSize);

class DescriptorPool {
public:
	typedef std::pair<uint32_t, VkDescriptorType> desc_type_count_t;
	DescriptorPool(const std::vector<desc_type_count_t>&, const std::vector<VkDescriptorSetLayout>&);
	~DescriptorPool();

	const DescriptorPool& write(size_t, uint32_t, VkDescriptorType, uint32_t, const std::vector<VkDescriptorBufferInfo>&) const;
	const DescriptorPool& write(size_t, uint32_t, VkDescriptorType, uint32_t, const std::vector<VkDescriptorImageInfo>&) const;
	const DescriptorPool& write(size_t, uint32_t, VkDescriptorType, uint32_t, const std::vector<VkBufferView>&) const;
	
	const std::vector<VkDescriptorSet>& sets() const;
private:
	std::vector<VkDescriptorSet> sets_;
	VkDescriptorPool pool_;
};

class Pipeline {
public:
	Pipeline(const std::vector<VkDescriptorSetLayout>& layouts);
	~Pipeline();
	std::vector<ShaderStage>& shaders();
	Pipeline& set(const std::vector<ShaderStage> &);
	Pipeline& set(const VertexInputState &);
	Pipeline& set(const ViewportState &);
	Pipeline& set(const RasterizationState &);
	Pipeline& set(const MultisampleState &);
	Pipeline& set(const DepthStencilState &);
	Pipeline& set(const ColorBlendState &);
	Pipeline& set(const DynamicState &);
	Pipeline& create();
	RenderPass& render_pass();
	VkPipeline handle() const;
	VkPipelineLayout layout() const;
private:
	PipelineLayout layout_;
	std::vector<ShaderStage> _shaders;
	VertexInputState _vertex_input;
	InputAssembly _input_assembly;
	ViewportState _viewport;
	RasterizationState _rasterization;
	MultisampleState _multisample;
	DepthStencilState _depth_stencil;
	ColorBlendState _color_blend;
	DynamicState _dynamic;
	RenderPass render_pass_;
	VkPipeline pipeline_;
};


#endif