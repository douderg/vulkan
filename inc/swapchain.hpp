#ifndef _SWAPCHAIN_HPP_
#define _SWAPCHAIN_HPP_

#include <vulkan/vulkan.hpp>
#include <vector>

class Swapchain {
public:
	Swapchain();
	~Swapchain();
	std::vector<VkImage> images() const;
	VkSurfaceFormatKHR image_format() const;
	VkSwapchainKHR handle() const;
	operator bool() const;
	VkExtent2D extent() const;
private:
	VkSurfaceFormatKHR image_format_;
	VkSwapchainKHR swapchain_;
	VkExtent2D extent_;
};

#endif