#ifndef _DEVICE_HPP_
#define _DEVICE_HPP_

#include <vulkan/vulkan.hpp>
#include <surface.hpp>

std::vector<VkPhysicalDevice> physical_devices(VkInstance);

class Device {
public:
	Device();
	~Device();
	VkDevice handle() const;
	const Surface& surface() const;
	operator bool() const;
	std::vector<VkSurfaceFormatKHR> supported_formats() const;
	std::vector<VkPresentModeKHR> present_modes() const;
	VkSurfaceCapabilitiesKHR surface_capabilities() const;
	VkPhysicalDeviceMemoryProperties memory_properties() const;
	std::vector<uint32_t> queue_indexes() const;
	std::vector<VkQueue> queues() const;
	void submit(const std::vector<VkSemaphore>&, const std::vector<VkSemaphore>&, const std::vector<VkCommandBuffer>&) const;
	void present(const std::vector<VkSemaphore>&) const;
private:
	std::vector<uint32_t> queue_indexes_;
	VkPhysicalDevice physical_device_;
	VkDevice device_;
	Surface surface_;
	std::vector<VkQueue> queues_;
};

class SubmitQueue {
public:
	SubmitQueue(VkQueue);
	SubmitQueue& waits(const std::vector<VkSemaphore>&);
	SubmitQueue& waits(const std::vector<VkPipelineStageFlags>&);
	SubmitQueue& fires(const std::vector<VkSemaphore>&);
	SubmitQueue& command_buffers(const std::vector<VkCommandBuffer>&);
	VkResult submit() const;
	VkResult submit(VkFence) const;
private:
	VkQueue queue_;
	std::vector<VkSemaphore> wait_, fire_;
	std::vector<VkPipelineStageFlags> stages_;
	std::vector<VkCommandBuffer> buffers_;
};

class PresentQueue {
public:
	PresentQueue(VkQueue);
	PresentQueue& waits(const std::vector<VkSemaphore>&);
	PresentQueue& swapchains(const std::vector<VkSwapchainKHR>&);
	PresentQueue& images(const std::vector<uint32_t>&);
	VkResult present() const;
private:
	VkQueue queue_;
	std::vector<VkSemaphore> wait_;
	std::vector<VkSwapchainKHR> swapchain_;
	std::vector<uint32_t> images_;
};

class Semaphore {
public:
	Semaphore();
	~Semaphore();
	VkSemaphore handle() const;
private:
	VkSemaphore semaphore_;
};

class Fence {
public:
	Fence();
	~Fence();
	VkFence handle() const;
	void wait() const;
private:
	VkFence fence_;
};

#endif