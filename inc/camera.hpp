#ifndef _CAMERA_HPP_
#define _CAMERA_HPP_

#include <chrono>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>

struct MVPMatrix {
	glm::mat4 m, v, p;
};

class Camera {
public:
	Camera();
	~Camera();
	MVPMatrix* ubo();
	Camera& perspective(float, float, float, float);
	Camera& look_at(glm::vec3, glm::vec3, glm::vec3);
	Camera& update();
private:
	MVPMatrix mvp_;
	std::chrono::high_resolution_clock::time_point start_;
};

#endif