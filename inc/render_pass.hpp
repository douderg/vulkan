#ifndef _RENDER_PASS_HPP_
#define _RENDER_PASS_HPP_

#include <vulkan/vulkan.hpp>
#include <vector>

class RenderPass {
public:
	RenderPass();
	~RenderPass();
	VkRenderPass handle() const;
	RenderPass& clear_values(const std::vector<VkClearValue>&);
	RenderPass& target(VkFramebuffer, VkOffset2D, VkExtent2D);
	RenderPass& commands(VkCommandBuffer);
	void begin() const;
private:
	VkRenderPass render_pass_;
	std::vector<VkClearValue> clear_values_;
	VkFramebuffer framebuffer_;
	VkOffset2D offset_;
	VkExtent2D extent_;
	VkCommandBuffer commands_;
};

#endif